package passCalculations;

/**
 * This class will perform all the calculations in a linear programing style.
 *
 * @author Timothy Ryer
 * @version 3.7
 */
public class Calculator {

    private int passNum;
    private double[] programs;
    private String[] assignments;

    /**
     * Default Constructor: Sets the variables to default values.
     */
    public Calculator() {
        passNum = 0;
        programs = null;
        assignments = null;
    }

    /**
     * Parameterized Constructor: Sets the variables if passed from object initialization.
     *
     * @param passes         Number of homework passes
     * @param totalPrograms  Array containing program grades
     * @param homeworkString Array containing homework grades and their point values
     */
    public Calculator(int passes, double[] totalPrograms, String[] homeworkString) {
        passNum = passes;
        programs = totalPrograms;
        assignments = homeworkString;
    }

    /**
     * Passes the variables to the Calculator class.
     *
     * @param passes         Number of homework passes
     * @param totalPrograms  Array containing program grades
     * @param homeworkString Array containing homework grades and their point values
     */
    public void transfer(int passes, double[] totalPrograms, String[] homeworkString) {
        passNum = passes;
        programs = totalPrograms;
        assignments = homeworkString;
    }

    /**
     * Applies a given number of Homework passes to Homework grades
     *
     * @param consume Number of Homework passes consumed
     * @return The patched string array.
     */
    public String[] homeworkCalc(int consume) {
        String[] patched = new String[assignments.length];
        for (int strange = 0; strange < assignments.length; strange++)
            patched[strange] = assignments[strange];
        for (int i = consume; i > 0; i--) {
            double biggest = 0;
            int index = 0;
            for (int x = 0; x < patched.length; x++)
                if (Double.parseDouble(patched[x].substring(patched[x].indexOf(" ") + 1)) - Double.parseDouble(patched[x].substring(0, patched[x].indexOf(" "))) > biggest) {
                    index = x;
                    biggest = Double.parseDouble(patched[x].substring(patched[x].indexOf(" ") + 1)) - Double.parseDouble(patched[x].substring(0, patched[x].indexOf(" ")));
                }
            patched[index] = patched[index].substring(patched[index].indexOf(" ") + 1);
            patched[index] += " " + patched[index];
        }
        return patched;
    }

    /**
     * Applies a given number of Homework passes to Program grades
     *
     * @param consume Number of Homework passes consumed
     * @return The patched integer array
     */
    public double[] programCalc(int consume) {
        double[] patched = new double[programs.length];
        for (int strange = 0; strange < programs.length; strange++)
            patched[strange] = programs[strange];
        for (int i = consume; i > 0; i--) {
            double smallest = 10;
            int index = 0;
            for (int x = 0; x < patched.length; x++)
                if (patched[x] < smallest) {
                    index = x;
                    smallest = patched[x];
                }
            patched[index] = 10;
        }
        return patched;
    }

    /**
     * Calculations based on the equation: f(x,y) = 0.15x + 0.2y
     *
     * @param homework String array with homework grades.
     * @param program  Integer array with program grades.
     * @return The calculated grade with the gives arrays.
     */
    public double finalGrade(String[] homework, double[] program) {
        //Finds sum of program array.
        double programTotal = 0;
        for (double p : program)
            programTotal += p;

        //Finds total points of homework array.
        int homeworkTotal = 0;
        for (String h : homework)
            homeworkTotal += Double.parseDouble(h.substring(h.indexOf(" ") + 1));

        //Returns the weighted average of the two point totals.
        return homeworkTotal * 0.15 + programTotal * 0.2;
    }

    /**
     * Finds which of the vertices of the feasible region will give the maximum grade.
     *
     * @return The values for the calc methods.
     */
    public String calculator() {
        int a = 0, b = 0;
        for (int x = 0; x <= passNum; x++)
            if (finalGrade(homeworkCalc(passNum - x), programCalc(x)) > finalGrade(homeworkCalc(a), programCalc(b))) {
                a = passNum - x;
                b = x;
            }
        return a + " " + b;
    }

    /**
     * Returns a sting with the results of all the calculations.
     */
    public String toString() {
        int hw = Integer.parseInt(calculator().substring(0, calculator().indexOf(" ")));
        int prog = Integer.parseInt(calculator().substring(calculator().indexOf(" ") + 1));
        String ans = "You should apply the HW Passes to these numerical grades: ";
        if (!PassRunner.progBug) {
            ans += "\nProgram(s): ";
            for (int x = 0; x < programs.length; x++)
                if (programCalc(prog)[x] != programs[x])
                    ans += programs[x] + " ";
        }
        if (!PassRunner.hwBug) {
            ans += "\nHomework Assignment(s): ";
            for (int y = 0; y < assignments.length; y++)
                if (!(homeworkCalc(hw)[y].equals(assignments[y])))
                    ans += "[" + assignments[y] + "] ";
        }
        return ans;
    }
}