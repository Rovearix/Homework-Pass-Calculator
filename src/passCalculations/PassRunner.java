package passCalculations;

import java.util.*;

/**
 * This class is the runner for the Homework Pass Calculator.
 *
 * @author Timothy Ryer
 * @version 2.7
 */
public class PassRunner {
    /**
     * Bug Fixes if grades = 0
     */
    public static boolean progBug = false, hwBug = false;

    public static void main(String[] args) {

        //Object initializations
        Calculator calc = new Calculator();
        Scanner keyboard = new Scanner(System.in);

        //Program start
        System.out.println("Welcome to the Dr. Hall Homework Pass Calculator!\n");
        System.out.print("To begin, enter the number of Homework Passes you have unlocked: ");
        int passes = keyboard.nextInt();
        if (passes <= 0) {
            System.out.print("Don't waste my time! >:(");
            return;
        }
        System.out.println((passes > 5) ? "Wow, good job!\n" : ""); // Congratulatory message

        //Inquire about grades
        System.out.println("Disclaimer: Eclipse programs are not the only program grades!");
        System.out.print("How many unsatisfactory program grades do you have? ");
        int badPrograms = keyboard.nextInt();
        if (badPrograms == 0) {
            progBug = true;
            badPrograms++;
        }
        System.out.print("How about homework grades? ");
        int badHomework = keyboard.nextInt();
        if (badHomework == 0) {
            hwBug = true;
            badHomework++;
        }
        System.out.println("");

        //Test the input for invalid answers
        if ((badPrograms < 0 || badHomework < 0) || (badPrograms <= 0 && badHomework <= 0)) {
            throw new IllegalArgumentException("That shouldn't happen, send me an email coach!");
        } else if (badPrograms + badHomework <= passes) {
            System.out.print("You have enough HW Passes to cover all these grades.");
            return;
        }

        //Program grade array
        double[] totalPrograms = new double[badPrograms];
        totalPrograms[0] = 0; //Error if null
        if (progBug)
            badPrograms = 0;
        for (int i = 0; i < badPrograms; i++) {
            System.out.print("What was the numerical grade for program " + (i + 1) + "? (out of 10.0) ");
            totalPrograms[i] = keyboard.nextDouble();
        }

        //Homework grade array
        String[] homeworkString = new String[badHomework];
        homeworkString[0] = "0 0"; //Error if null
        if (!hwBug)
            System.out.println("\nHomework grades are typed like: \"grade total\" Ex. \"5.0 10.0\"");
        else
            badHomework = 0;
        keyboard.nextLine(); //Used to prevent line skipping
        for (int x = 0; x < badHomework; x++) {
            System.out.print("Grade for homework " + (x + 1) + "? ");
            homeworkString[x] = keyboard.nextLine();
        }
        System.out.println();

        //Pass the work onto the Calculator class
        calc.transfer(passes, totalPrograms, homeworkString);
        System.out.print(calc);
    }
}